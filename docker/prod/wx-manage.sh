#!/bin/bash

# version
APP_NAME="wx-manage"
CONTAINER_NAMES=("${APP_NAME}-01")
REGISTRY="registry.cn-shenzhen.aliyuncs.com"
HUB_USR="guanhai@msn.com"
HUB_PWD="Up@202401292215"
REGISTRY_SPACE="${REGISTRY}/up-juvenile"
VERSION=$1

deploy() {
  # 1. docker login
  docker login -u "${HUB_USR}" -p "${HUB_PWD}" "${REGISTRY}"

  echo ${REGISTRY_SPACE}/${APP_NAME}:$VERSION
  # 2. docker pull 拉取镜像
  docker pull ${REGISTRY_SPACE}/${APP_NAME}:$VERSION

  # 3. query exist container, loop query
  # shellcheck disable=SC2068
  for container_name in ${CONTAINER_NAMES[@]}
  do
    echo "start to deploy ${container_name}"
    if [[ "${container_name}" == "${APP_NAME}-01" ]]; then
      _port1=14080
      # _port2=443
     # elif [[ "${container_name}" == "lockgate-mall-admin-api-02" ]]; then
     #   _port1=8084
     #  _port2=9994
    else
      echo "error container_name"
      read -n 1 anykey
      exit
    fi

    # stop & rm exists
    docker ps -a | grep "${container_name}" | awk '{printf $1}' | xargs docker stop | xargs docker rm || true
    # run a new one
    # @20240131 加本地文件存储路径
    # @20240201 加minio代理地址
    docker run -d \
      --name "${container_name}" \
      -p ${_port1}:80 \
      -v /opt/app/${APP_NAME}/"${container_name}"/logs:/var/logs/nginx \
      -v /opt/app/extends:/opt/app/extends \
      -e TZ="Asia/Shanghai" \
      -e NGINX_GATEWAY_URL="http://10.1.46.15:14088" \
      --restart=always \
      --privileged=true \
      ${REGISTRY_SPACE}/${APP_NAME}:$VERSION
  done
}

deploy
